﻿
# Sistemas de Ponto Flutuante

### Introdução

Os sistemas de ponto flutuante são um formato de representação de números racionais que são utilizados em computadores, esta forma de representação permite que tanto números pequenos quanto grandes sejam representados pelo computador.

### Representação

Nos sistemas de pontos flutuantes os números são representados da seguinte forma:

$$
num = \pm m * b ^ {\pm e}
$$

Onde:

 - **m** = mantissa $(0.d_1d_2d_3...d_n) (d_1\ne0)$
 - **b** = base $(2, 8, 10, 16)$
 - **e** = expoente

Abaixo temos alguns exemplos de números sendo representados na forma do sistema de ponto flutuante:

$$
235.89_{(10)} = 0.23589 * 10 ^ 3
$$
$$
101.01_{(2)} = 0.10101 * 2 ^ {11}
$$
$$
0.000875_{(10)} = 0.875 * 10 ^ {-3}
$$

Um sistema de ponto flutuante pode ser definido da seguinte forma:

$$
SPF(b, n, exp_{min}, exp_{max})
$$

Onde:

 - **b** = base (2,8,10,16)
 - **n** = número de dígitos da mantissa
 - $\boldsymbol{exp_{min}}$ = limite inferior do expoente
 - $\boldsymbol{exp_{max}}$ = limite superior do expoente

Todo número representável por esse sistema é capaz de ser escrito no formato mencionado anteriormente obedecendo os valores definido por esse sistema. 

Por exemplo, imagine o seguinte sistema de ponto flutuante:

$$
SPF(10, 3, -5, 5)
$$

O número $235_{(10)} = 0.235 * 10 ^ 3$ é considerado um número representável por esse sistema, pois sua base é 10, sua mantissa possui 3 dígitos e seu expoente é 3, ou seja, todos esses valores são aceitos pelo sistema, a base e o número de dígitos da mantissa são iguais ao do sistema e o expoente possui um valor entre os limites inferior e superior.

### Underflow e Overflow

Quando um número está sendo representado por um sistema, o valor do seu expoente precisa obedecer os limites inferior e superior definidos, quando isso não acontece classificamos esses casos como:

**Underflow:** quando o expoente do número sendo representado é menor que o limite inferior do expoente do sistema de ponto flutuante.

**Overflow:** quando o expoente do número sendo representado é maior que o limite superior do expoente do sistema de ponto flutuante.

Por exemplo, imagine o seguinte sistema de ponto flutuante:

$$
SPF(10, 2, -5, 5)
$$

Vamos tentar representar neste sistema os números $0.000000073_{(10)}$ e $7300000_{(10)}$.

$0.000000073_{(10)} = 0.73 * 10 ^ {-7}$,  como -7 é menor que -5, ou seja, o expoente do número sendo representado é menor que o limite inferior do expoente do sistema, dizemos que ocorreu um caso de **underflow**.

$7300000_{(10)} = 0.73 * 10 ^ {7}$,  como 7 é maior que 5, ou seja, o expoente do número sendo representado é maior que o limite superior do expoente do sistema, dizemos que ocorreu um caso de **overflow**.

Em ambos os casos, o número não consegue ser representado pelo sistema.

### Truncamento e Arredondamento

Outro problema que acontece quando um número está sendo representado em um sistema de ponto flutuante é que o número de dígitos de sua mantissa pode ser superior ao número esperado, quando isso acontece um processo de  **truncamento** ou **arredondamento** é utilizado para permitir a representação desse número.

O **truncamento** é bem simples, todos os dígitos da mantissa que passam do número de dígitos esperado são excluídos. Por exemplo, imagine que o sistema possui um **n** igual a 3, ou seja, a mantissa dos números representados por esse sistema possuem 3 dígitos, e que queremos representar o número abaixo:

$$
235.89_{(10)} = 0.23589 * 10 ^ 3
$$

Como esse número possui 5 dígitos na mantissa, todos os dígitos que excedem o número limite do sistema serão excluídos, chegando ao resultado final:

$$
235.89_{(10)} = 0.235 * 10 ^ 3
$$

Que é a representação desse número para esse sistema.

Agora no caso do **arredondamento**, o último dígito que está dentro do limite poderá ser alterado com base no dígito seguinte, que no caso é o primeiro dígito que será excluído. Existem três casos dependendo do valor do dígito seguinte, estes são os casos:

 - Caso o dígito seguinte seja maior que 5, o último digito deve receber um +1
 - Caso o dígito seguinte seja menor que 5, o último dígito deve ser mantido
 - Caso o dígito seguinte seja igual a 5, o último dígito só receberá um +1 se ele for ímpar, se ele for par ele deve ser mantido.

Imagine que aquele mesmo sistema anterior (**n** = 3) esteja sendo utilizado e você quer representar neste sistema os números abaixo:

 $$
 235.89_{(10)} = 0.23589 * 10 ^ 3
 $$
 $$
 235.39_{(10)} = 0.23539 * 10 ^ 3
 $$
 $$
 235.59_{(10)} = 0.23559 * 10 ^ 3
 $$

No primeiro caso, como o último dígito é o 5 e o dígito seguinte é 8, o 5 receberá um +1, resultando em:

 $$
 235.89_{(10)} = 0.236 * 10 ^ 3
 $$

No segundo caso, como o último dígito é o 5 e o dígito seguinte é 3, o 5 será mantido, resultando em:

 $$
 235.39_{(10)} = 0.235 * 10 ^ 3
 $$

Já no terceiro caso, como o último dígito é o 5 e o dígito seguinte é 5, temos que avaliar se  o último dígito é par ou ímpar, neste caso ele é ímpar, logo o 5 receberá um +1, resultando em:

 $$
 235.59_{(10)} = 0.236 * 10 ^ 3
 $$

É importante mencionar que quando a mantissa tiver menos números que o valor de **n** do sistema, devem ser adicionados zeros à direita do último dígito, por exemplo:

 $$
 23_{(10)} = 0.230 * 10 ^ 2
 $$

