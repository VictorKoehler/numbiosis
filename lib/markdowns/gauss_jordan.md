# Método de Gauss Jordan

## Definição
É um método criado para resolução de sistemas de equações lineares, sendo uma derivação da Eliminação Gaussiana. De forma bastante semelhante, as equações serão trabalhadas na forma de matriz estendida, e operações elementais devem ser aplicadas nas respectivas linhas dessas matrizes com o intuito de transforma-la em uma matriz escalonada reduzida, ou seja, zerando todos os coeficientes da matriz principal, exceto a diagonal que possuirá coeficiente um.


## Passos

*  De forma inicial, é necessário transformar o sistema linear em forma de matriz, dita matriz estendida, usando os coeficientes das variáveis como elementos dessa matriz.
$$
\begin{array}{c}a_{11}x_1+a_{12}x_2+a_{13}x_3 = b_1 \quad(L_1)\\
a_{21}x_1+a_{22}x_2+a_{23}x_3 = b_2 \quad(L_2)\\
a_{31}x_1+a_{32}x_2+a_{33}x_3 = b_3 \quad(L_3)\\
\downarrow\\
\left[
\begin{array}{ccc|c}
	a_{11}&a_{12}&a_{13}&b_{1}\\
	a_{21}&a_{22}&a_{23}&b_{2}\\
	a_{31}&a_{32}&a_{33}&b_{3}
\end{array}
\right]
\end{array}
$$

* É necessário conhecer algumas operações elementares, sendo elas a multiplicação de uma linha da matriz por uma constante não-nula, a substituição de uma linha por ela mesma somada a um múltiplo de outra linha e a permutação entre linhas.
$$
\begin{array}{c}Multiplicação\ por\ uma\ constante:\\\\
L_2 \leftarrow L_2 .w\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{11}&a_{12}\\
w.a_{21}&w.a_{22}\\
\end{bmatrix}
\end{array}
$$

$$
\begin{array}{c}Soma\ com\ o\ multiplo\ de\ outra\ linha:\\\\
L_2 \leftarrow L_2 - L_1.w\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}-a_{11}.w&a_{22}-a_{12}.w\\
\end{bmatrix}
\end{array}
$$

$$
\begin{array}{c}Permutação\ de\ linhas:\\\\
L_2 \leftarrow L_1\ e\ L_1\leftarrow L_2\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{21}&a_{22}\\
a_{11}&a_{12}\\
\end{bmatrix}
\end{array}
$$

* Sabendo as operações, as linhas das matrizes serão trabalhadas à fim de obter todos os coeficientes nulos, exceto a diagonal principal que deve ser manipulado para que possua valor unitário.
$$
\left[
\begin{array}{ccc|c}
	a_{11}&a_{12}&a_{13}&b_{1}\\
	a_{21}&a_{22}&a_{23}&b_{2}\\
	a_{31}&a_{32}&a_{33}&b_{3}
\end{array}
\right]\ \rightarrow\ 
\left[
\begin{array}{ccc|c}
	1&0&0&b_{1}^{(k)}\\
	0&1&0&b_{2}^{(k)}\\
	0&0&1&b_{3}^{(k)}
\end{array}
\right]
$$

* Tendo a matriz com somente a diagonal principal com valores, é possível encontrar de forma direta o resultado as variáveis, sem precisar substituir nas equações e resolve-las. Está é a parte onde o método se difere da eliminação gaussiana, pois a equação já está resolvida diretamente, sem necessidade de sucessivas substituições.
$$
\left[
\begin{array}{ccc|c}
	1&0&0&b_{1}^{(k)}\\
	0&1&0&b_{2}^{(k)}\\
	0&0&1&b_{3}^{(k)}
\end{array}
\right]\ \rightarrow\ 
\left\{
\begin{array}{c}
x_1 = b_{1}^{(k)}\\
x_2 = b_{2}^{(k)}\\
x_3 = b_{3}^{(k)}\\
\end{array}
\right. \blacksquare
$$