export default [
  {
    sectionName: "Sistemas de numeração",
    items: [
      {
        name: "Ponto Flutuante",
        route: "/ponto-flutuante"
      },
      {
        name: "Conversão de base",
        route: "/base-numerica"
      }
    ]
  },
  {
    sectionName: "Raiz de função",
    items: [
      {
        name: "Bisseção",
        route: "/bissecao"
      },
      {
        name: "Secante",
        route: "/secante"
      },
      {
        name: "Falsa posição",
        route: "/falsa-posicao"
      },
      {
        name: "Muller",
        route: "/muller"
      },
      {
        name: "Newton-Raphson",
        route: "/newton_raphson"
      }
    ]
  },
  {
    sectionName: "Sistemas lineares",
    items: [
      {
        name: "Eliminação de Gauss",
        route: "/gauss"
      },
      {
        name: "Gauss-Jordan",
        route: "/gauss-jordan"
      },
      {
        name: "Gauss-Jacobi",
        route: "/gauss-jacobi"
      },
      {
        name: "Gauss-Seidel",
        route: "/gauss-seidel"
      },
      {
        name: "Cholesky",
        route: "/cholesky"
      }
    ]
  },
  {
    sectionName: "Sistemas não-lineares",
    items: [
      {
        name: "Newton",
        route: "/newton"
      }
    ]
  },
  {
    sectionName: "Interpolação",
    items: [
      {
        name: "Spline cúbica",
        route: "/spline-cubica"
      },
      {
        name: "Método de Newton",
        route: "/interpolador-newton"
      }
    ]
  },
  {
    sectionName: "Equações Diferenciais",
    items: [
      {
        name: "Métodos de Runge Kutta",
        route: "/runge-kutta"
      }
    ]
  }
]
