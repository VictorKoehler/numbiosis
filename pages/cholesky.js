import Cholesky from "../components/Cholesky"
import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/cholesky.md")} />
    <h2></h2>
    <Cholesky />
  </MethodsLayout>
)