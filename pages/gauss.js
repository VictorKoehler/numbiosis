import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"
import React from "react"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/eliminacao_gauss.md")} />
  </MethodsLayout>
)
