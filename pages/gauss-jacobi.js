import GaussJordan from "../components/GaussJordan"
import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/gauss_jacobi.md")} />
  </MethodsLayout>
)
